//
//  AppDelegate.h
//  Silent Push Badge + 1
//
//  Created by Frain on 16/9/29.
//  Copyright © 2016年 com.frain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

