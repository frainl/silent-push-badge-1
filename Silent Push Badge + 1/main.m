//
//  main.m
//  Silent Push Badge + 1
//
//  Created by Frain on 16/9/29.
//  Copyright © 2016年 com.frain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
